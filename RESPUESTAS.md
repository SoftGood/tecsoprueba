
-SOLUCIONES
Para darle solucion a los ejercicios planteados, todas las petciones de entrada y salida se procesan en formato JSON.


Acontinuacion se describen las rutas y datas para acceder a las funcionalidas:


- Obtener las cuentas registradas.
	curl --location --request GET 'http://localhost:8080/cuentas/obtenerCuentas'
	
- Crear una nueva cuenta.

  curl --location --request GET 'http://localhost:8080/cuentas/crearCuenta' \
  --header 'Content-Type: application/json' \
  --data-raw '{
	"nuemerocuenta": 2343434543,
	"moneda": "dolar",
	"saldo": 1400
    }'
    
- Obtener movimientos.

  curl --location --request GET 'http://localhost:8080/cuentas/obtenerMovimientos'
  
  
- Crear movimiento.

 curl --location --request GET 'http://localhost:8080/cuentas/crearMovimiento' \
 --header 'Content-Type: application/json' \
 --data-raw '{
	"tipomovimiento": "debito",
	"descripcion": "compra de prueba",
	"importe": 10.00,
	"cuenta": {
		        "nuemerocuenta": 1222334444334
		      }
   }'
   
   
- Eliminar cuenta

  curl --location --request GET 'http://localhost:8080/cuentas/eliminarCuenta' \
  --header 'Content-Type: application/json' \
  --data-raw '1222334444334'
  
  