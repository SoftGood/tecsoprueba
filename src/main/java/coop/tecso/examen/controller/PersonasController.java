package coop.tecso.examen.controller;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import coop.tecso.examen.dto.RespuestaDto;
import coop.tecso.examen.model.Personas;
import coop.tecso.examen.service.impl.PersonaServiceImpl;

@RestController
@RequestMapping(path = "/personas")
public class PersonasController {

	@Autowired
	private PersonaServiceImpl personaService;
	private static final Logger logger = LogManager.getLogger(PersonasController.class);

	private final static String MENSAJEERROR = "Se presentó un problema ejecutando la operación";

	@GetMapping(path = "/obtenerPersonas", produces = MediaType.APPLICATION_JSON)
	@CrossOrigin
	public RespuestaDto<Personas> obtenerPersonas() {
		RespuestaDto<Personas> respuesta = new RespuestaDto<Personas>();
		try {

			respuesta.setExitoso(true);
			respuesta.setData(personaService.obtenerPersonas());

		} catch (Exception e) {
			respuesta.setExitoso(false);
			respuesta.setMensaje(MENSAJEERROR);
			logger.info(e.getMessage());
		}
		return respuesta;

	}

	@PostMapping(path = "/crearPersona", produces = MediaType.APPLICATION_JSON)
	@CrossOrigin
	public RespuestaDto<Personas> crearPersona(@RequestBody Personas peticion) {
		RespuestaDto<Personas> respuesta = new RespuestaDto<Personas>();
		try {

			if (peticion != null && personaService.consultarPersona(peticion.getRut()) == null) {
			
				personaService.crearPersona(peticion);
				
				respuesta.setExitoso(true);
				
				respuesta.setMensaje("Se creo con éxito la persona");
			} else {
				respuesta.setExitoso(false);
				respuesta.setMensaje("Ya existe una persona con el rut ingresado.");
			}
			

		} catch (Exception e) {
			respuesta.setExitoso(false);
			respuesta.setMensaje(MENSAJEERROR);
			logger.info(e.getMessage());
		}
		return respuesta;

	}
	
	
	
	@PostMapping(path = "/editarPersona", produces = MediaType.APPLICATION_JSON)
	@CrossOrigin
	public RespuestaDto<Personas> editarPersona(@RequestBody Personas peticion) {
		RespuestaDto<Personas> respuesta = new RespuestaDto<Personas>();
		try {

			if (peticion != null && personaService.consultarPersona(peticion.getRut()) != null) {
			
				personaService.editarPersona(peticion);
				
				respuesta.setExitoso(true);
				respuesta.setMensaje("Se actualizó con éxito la persona");
			} else {
				respuesta.setExitoso(false);
				respuesta.setMensaje("La persona a modificar ya no se encuentra disponible.");
			}
			

		} catch (Exception e) {
			respuesta.setExitoso(false);
			respuesta.setMensaje(MENSAJEERROR);
			logger.info(e.getMessage());
		}
		return respuesta;

	}
	
	
	
	@PostMapping(path = "/eliminarPersona", produces = MediaType.APPLICATION_JSON)
	@CrossOrigin
	public RespuestaDto<Personas> eliminarPersona(@RequestBody Long peticion) {
		RespuestaDto<Personas> respuesta = new RespuestaDto<Personas>();
		try {

			if (peticion != null && personaService.consultarPersona(peticion) != null) {
			
				personaService.eliminarPersona(peticion);
				
				respuesta.setExitoso(true);
				respuesta.setMensaje("Se eliminó con éxito la persona");
			} else {
				respuesta.setExitoso(false);
				respuesta.setMensaje("La persona a eliminar ya no se encuentra disponible.");
			}
			

		} catch (Exception e) {
			respuesta.setExitoso(false);
			respuesta.setMensaje(MENSAJEERROR);
			logger.info(e.getMessage());
		}
		return respuesta;

	}
}
