package coop.tecso.examen.controller;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import coop.tecso.examen.dto.RespuestaDto;
import coop.tecso.examen.model.Cuentas;
import coop.tecso.examen.model.Movimientos;
import coop.tecso.examen.service.impl.CuentaServiceImpl;
import coop.tecso.examen.service.impl.MovimientoServiceImpl;

@RestController
@RequestMapping(path = "/cuentas")
public class CuentasController {

	@Autowired
	private CuentaServiceImpl cuentaService;

	@Autowired
	private MovimientoServiceImpl movimientoService;

	private static final Logger logger = LogManager.getLogger(CuentasController.class);

	private final static String MENSAJEERROR = "Se presentó un problema ejecutando la operación";
	private final static String MENSAJEXITOSO = "Operación finalizada con éxito.";

	@GetMapping(path = "/obtenerCuentas", produces = MediaType.APPLICATION_JSON)
	@CrossOrigin
	public RespuestaDto<Cuentas> obtenerCuentas() {
		RespuestaDto<Cuentas> respuesta = new RespuestaDto<Cuentas>();
		try {

			respuesta.setExitoso(true);
			respuesta.setData(cuentaService.obtenerCuentas());

		} catch (Exception e) {
			respuesta.setExitoso(false);
			respuesta.setMensaje(MENSAJEERROR);
			logger.info(e.getMessage());
		}

		return respuesta;
	}

	@GetMapping(path = "/crearCuenta", produces = MediaType.APPLICATION_JSON)
	@CrossOrigin
	public RespuestaDto<Cuentas> crearCuenta(@RequestBody Cuentas peticion) {
		RespuestaDto<Cuentas> respuesta = new RespuestaDto<Cuentas>();
		try {

			if (peticion != null) {

				if (cuentaService.buscarCuenta(peticion.getNuemerocuenta()) == null) {

					cuentaService.crearCuenta(peticion);
					respuesta.setExitoso(true);
					respuesta.setMensaje(MENSAJEXITOSO);
				} else {
					respuesta.setExitoso(false);
					respuesta.setMensaje("Ya existe una cuenta con el numero ingresado.");
				}

			} else {
				respuesta.setExitoso(false);
				respuesta.setMensaje("La información para la cuenta no puede estar vacia.");
			}

		} catch (Exception e) {
			respuesta.setExitoso(false);
			respuesta.setMensaje(MENSAJEERROR);
			logger.info(e.getMessage());
		}

		return respuesta;
	}

	@GetMapping(path = "/eliminarCuenta", produces = MediaType.APPLICATION_JSON)
	@CrossOrigin
	public RespuestaDto<Cuentas> eliminarCuenta(@RequestBody Long peticion) {
		RespuestaDto<Cuentas> respuesta = new RespuestaDto<Cuentas>();
		try {

			if (peticion != null) {

				Cuentas cuenta = cuentaService.buscarCuenta(peticion);
				if (cuenta != null) {

					if (movimientoService.movimientosPorCuenta(cuenta).isEmpty()) {

						cuentaService.eliminarCuenta(peticion);
						respuesta.setExitoso(true);
						respuesta.setMensaje(MENSAJEXITOSO);
					} else {
						respuesta.setExitoso(false);
						respuesta.setMensaje("No se puede eliminar esta cuenta porque tiene movimientos asociados.");
					}

				} else {
					respuesta.setExitoso(false);
					respuesta.setMensaje("La cuenta ha eliminar no existe.");
				}

			} else {
				respuesta.setExitoso(false);
				respuesta.setMensaje("Debe ingresar una cuenta válida.");
			}

		} catch (Exception e) {
			respuesta.setExitoso(false);
			respuesta.setMensaje(MENSAJEERROR);
			logger.info(e.getMessage());
		}

		return respuesta;
	}

	@GetMapping(path = "/obtenerMovimientos", produces = MediaType.APPLICATION_JSON)
	@CrossOrigin
	public RespuestaDto<Movimientos> obtenerMovimientos() {
		RespuestaDto<Movimientos> respuesta = new RespuestaDto<Movimientos>();
		try {

			List<Movimientos> listar = movimientoService.obtenerMovimientos();

			if (!listar.isEmpty()) {
				respuesta.setExitoso(true);
				respuesta.setData(listar);
			} else {
				respuesta.setExitoso(true);
				respuesta.setMensaje("No se encontraron movimientos disponibles para visualizar");
			}

		} catch (Exception e) {
			respuesta.setExitoso(false);
			respuesta.setMensaje(MENSAJEERROR);
			logger.info(e.getMessage());
		}

		return respuesta;
	}

	@GetMapping(path = "/crearMovimiento", produces = MediaType.APPLICATION_JSON)
	@CrossOrigin
	public RespuestaDto<Movimientos> crearMovimiento(@RequestBody Movimientos peticion) {
		RespuestaDto<Movimientos> respuesta = new RespuestaDto<Movimientos>();
		try {

			if (peticion != null) {

				Cuentas cuenta = cuentaService.buscarCuenta(peticion.getCuenta().getNuemerocuenta());

				if (cuenta != null) {
					if (validarMovimiento(cuenta.getMoneda(), peticion.getImporte())) {

						if (cuenta.getSaldo() >= peticion.getImporte()) {
							cuenta.setSaldo(cuenta.getSaldo() - peticion.getImporte());

							peticion.setCuenta(cuenta);

							movimientoService.agregarMovimiento(peticion);
							respuesta.setExitoso(true);
							respuesta.setMensaje(MENSAJEXITOSO);
						} else {
							respuesta.setExitoso(false);
							respuesta.setMensaje("No cuenta con saldo suficiente para esta operación.");
						}
					} else {
						respuesta.setExitoso(false);
						respuesta.setMensaje("Transacción rechazada.");
					}
				} else {
					respuesta.setExitoso(false);
					respuesta.setMensaje("La cuenta ingresada no existe.");
				}

			} else {
				respuesta.setExitoso(false);
				respuesta.setMensaje("Ya existe una cuenta con el número ingresado.");
			}

		} catch (Exception e) {
			respuesta.setExitoso(false);
			respuesta.setMensaje(MENSAJEERROR);
			logger.info(e.getMessage());
		}

		return respuesta;
	}

	private boolean validarMovimiento(String moneda, Double valor) {

		boolean valido = true;

		if (moneda.equals("peso") && valor > 1000) {
			valido = false;
		} else if (moneda.equals("dolar") && valor > 300) {
			valido = false;
		} else if (moneda.equals("euro") && valor > 150) {
			valido = false;
		}

		return valido;

	}

}
