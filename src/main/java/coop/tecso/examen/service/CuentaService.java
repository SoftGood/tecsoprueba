package coop.tecso.examen.service;

import java.util.List;

import coop.tecso.examen.model.Cuentas;

public interface CuentaService {

	public List<Cuentas> obtenerCuentas();

	public void crearCuenta(Cuentas cuentas);

	public void eliminarCuenta(Long numerocuenta);
	
	public Cuentas buscarCuenta(Long numerocuenta);

}
