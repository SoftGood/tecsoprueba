package coop.tecso.examen.service;

import java.util.List;

import coop.tecso.examen.model.Personas;

public interface PersonaService {
	
	public List<Personas> obtenerPersonas();
	
	public void crearPersona(Personas persona);
	
	public void editarPersona(Personas persona);
	
	public void eliminarPersona(Long rut);
	
	public Personas consultarPersona(Long rut);
	
	
}
