package coop.tecso.examen.service;

import java.util.List;

import coop.tecso.examen.model.Cuentas;
import coop.tecso.examen.model.Movimientos;

public interface MovimientoService {

	public List<Movimientos> obtenerMovimientos();

	public void agregarMovimiento(Movimientos movimiento);
	
	public List<Movimientos> movimientosPorCuenta(Cuentas cuenta);
	
}
