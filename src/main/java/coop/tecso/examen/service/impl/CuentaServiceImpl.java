package coop.tecso.examen.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import coop.tecso.examen.model.Cuentas;
import coop.tecso.examen.repository.CuentasRepository;
import coop.tecso.examen.service.CuentaService;

@Service
public class CuentaServiceImpl implements CuentaService {

	@Autowired
	private CuentasRepository cuentasRespository;

	@Override
	public List<Cuentas> obtenerCuentas() {
		return cuentasRespository.findAll();
	}

	@Override
	public void crearCuenta(Cuentas cuentas) {
		cuentasRespository.save(cuentas);
	}

	@Override
	public void eliminarCuenta(Long numerocuenta) {
		cuentasRespository.delete(numerocuenta);

	}

	@Override
	public Cuentas buscarCuenta(Long numerocuenta) {
		return cuentasRespository.findOne(numerocuenta);
	}

}
