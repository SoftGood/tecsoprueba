package coop.tecso.examen.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import coop.tecso.examen.model.Personas;
import coop.tecso.examen.repository.PersonasRepository;
import coop.tecso.examen.service.PersonaService;

@Service
public class PersonaServiceImpl implements PersonaService {

	@Autowired
	private PersonasRepository personas;

	@Override
	public List<Personas> obtenerPersonas() {
		return personas.findAll();
	}

	@Override
	public void crearPersona(Personas persona) {

		 personas.saveAndFlush(persona);
	}

	@Override
	public void editarPersona(Personas persona) {
		 personas.saveAndFlush(persona);
	}

	@Override
	public void eliminarPersona(Long rut) {
		personas.delete(rut);
	}

	@Override
	public Personas consultarPersona(Long rut) {
		return personas.findOne(rut);
	}

}
