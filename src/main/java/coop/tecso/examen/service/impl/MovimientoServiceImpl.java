package coop.tecso.examen.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import coop.tecso.examen.model.Cuentas;
import coop.tecso.examen.model.Movimientos;
import coop.tecso.examen.repository.MovimientosRepository;
import coop.tecso.examen.service.MovimientoService;

@Service
public class MovimientoServiceImpl implements MovimientoService {

	@Autowired
	private MovimientosRepository moviRepository;

	@Override
	public List<Movimientos> obtenerMovimientos() {
		return moviRepository.findAll();
	}

	@Override
	public void agregarMovimiento(Movimientos movimiento) {
		moviRepository.save(movimiento);

	}

	@Override
	public List<Movimientos> movimientosPorCuenta(Cuentas cuenta) {
		return moviRepository.movimientosPorCuenta(cuenta);
	}

}
