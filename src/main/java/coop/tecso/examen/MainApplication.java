package coop.tecso.examen;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;


@SpringBootApplication
@SpringBootConfiguration
@Configuration
@EnableAsync
@ComponentScan(basePackages = "coop.tecso.examen.*")
@EntityScan("coop.tecso.examen.model")
@EnableJpaRepositories("coop.tecso.examen.repository")

public class MainApplication extends AsyncConfigurerSupport {

	private static final  Logger logger = LogManager.getLogger(MainApplication.class);

	@Bean
	public CountDownLatch closeLatch() {
		return new CountDownLatch(1);
	}
	
    
	public static void main(String[] args) throws InterruptedException {
		ApplicationContext ctx = SpringApplication.run(MainApplication.class, args);
		logger.info("Se inicio con exito el servicio");
		CountDownLatch closeLatch = ctx.getBean(CountDownLatch.class);
		closeLatch.await();
	}
	
	

	@Override
	public Executor getAsyncExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(90000);
		executor.setMaxPoolSize(100000);
		executor.setThreadNamePrefix("restApi-");
		executor.initialize();
		return executor;
	}
	
}
