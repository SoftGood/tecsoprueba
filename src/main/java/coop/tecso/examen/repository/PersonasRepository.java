package coop.tecso.examen.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import coop.tecso.examen.model.Personas;

@Repository
public interface PersonasRepository extends  JpaRepository<Personas, Long> {}
