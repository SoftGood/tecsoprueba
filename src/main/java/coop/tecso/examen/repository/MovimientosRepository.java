package coop.tecso.examen.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import coop.tecso.examen.model.Cuentas;
import coop.tecso.examen.model.Movimientos;

@Repository
public interface MovimientosRepository extends JpaRepository<Movimientos, Long> {
	
	@Query("SELECT m FROM Movimientos m WHERE m.cuenta = :pCuenta")
	List<Movimientos> movimientosPorCuenta(@Param("pCuenta") Cuentas cuenta);
}


