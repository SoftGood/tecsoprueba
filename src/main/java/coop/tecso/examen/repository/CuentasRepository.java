package coop.tecso.examen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import coop.tecso.examen.model.Cuentas;

@Repository
public interface CuentasRepository extends  JpaRepository<Cuentas, Long> { }
