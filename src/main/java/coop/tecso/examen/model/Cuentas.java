package coop.tecso.examen.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Cuentas implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2537039256432831303L;
	
	private Long nuemerocuenta;
	private String moneda;
	private Double saldo;

	@Id
	@Column(nullable = false)
	public Long getNuemerocuenta() {
		return nuemerocuenta;
	}

	public void setNuemerocuenta(Long nuemerocuenta) {
		this.nuemerocuenta = nuemerocuenta;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	@Column(precision=10, scale=2)
	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

}
