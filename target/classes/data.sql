INSERT INTO country (iso_code, name, creation_timestamp, modification_timestamp, version_number) 
VALUES ('AR','ARGENTINA', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO country (iso_code, name, creation_timestamp, modification_timestamp, version_number) 
VALUES ('BR','BRASIL', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO country (iso_code, name, creation_timestamp, modification_timestamp, version_number) 
VALUES ('UY','URUGUAY', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);

INSERT INTO country (iso_code, name, creation_timestamp, modification_timestamp, version_number) 
VALUES ('CH','CHILE', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);



INSERT INTO personas (rut, identificacion, nombre, apellidos, razonsocial, aniofundacion, fechamodificacion, fechacreacion) 
VALUES ('123456789', 12345678865, 'Juan','Perez', '',2019, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());


INSERT INTO Cuentas (nuemerocuenta, moneda, saldo) 
VALUES ('1222334444334','peso', 5000);